#!/bin/sh
# Author: Anderson Moreira
# Run postmark and write a configuration file
#tar -xvf postmark_1.51.orig.tar.gz
#cd postmark-1.51/
#cc postmark-1.51.c -o postmark
#echo $? > ~/install-exit-status
cd Debug
#echo "#!/bin/sh
#cd postmark-1.51/
echo "set transactions 1000000" > config.pmrc
echo "set size 256 512" >> config.pmrc
echo "set number 10000" >> config.pmrc
echo "run" >> config.pmrc
#echo "quit\" >> config.pmrc
./Postmark config.pmrc > LOG_FILE 2>&1
