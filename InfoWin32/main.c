/*
* main.cpp
*
*  Created on: 26/04/2017
*      Author: Anderson
*/

#include "info.h"

int main() {
	FILE* log = stdout;
	char buffer[255] = { 0 };

	log = fopen("dinamicLog.dat", "w");
	fprintf(log, "Clock Processor %d\n", getClockProcessador()); //imprime no arquivo o valor do clock do processador
	fprintf(log, "%s\n", infoProcessador(buffer)); //imprime no arquivo o string do processador
	fprintf(log, "HDD Size %.0f\n", infoDisk()); // imprime no arquivo o tamanho do Hd
	fprintf(log, "RAM Size %u\n", infoMem()); //imprime no arquivo o tamanho de memoria ram instalada
	fprintf(log, "Graphic Size %i\n", infoVideo()); //imprime no arquivo o valor da memoria de video
	memset((void*)buffer, 0, 255);
	fprintf(log, "Video %s\n", infoVideoString(buffer)); // imprime no arquivo o string da placa de video

	fclose(log);

	return 0;
}
