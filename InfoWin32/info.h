/*
 ============================================================================
 Name        : info.h
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 27 de abr de 2017
 Copyright   : 
 Description : 
 ============================================================================
 */
#ifndef INFO_H_
#define INFO_H_

#include <windows.h>
#include <stdio.h>
#include <math.h>

#define KEY_PROCESSADOR "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0"
//#define KEY_BRAND "HARDWARE\\DESCRIPTION\\BIOS" //TO-DO
#define KEY_VIDEO "HARDWARE\\DEVICEMAP\\VIDEO"

unsigned long infoMem();
float infoDisk();
char* infoProcessador(char* buffer);
int infoVideo();
char* infoVideoString(char* retBuf);
long getClockProcessador();
//char* infoBrand(char *buffer); //TO-DO


/************************************* funcao que retorna tamanho da memoria Ram**********************************/

unsigned long infoMem() {
	MEMORYSTATUS stat;
	GlobalMemoryStatus(&stat);
	return (unsigned long)stat.dwTotalPhys / (1024 *1024); //retorna em Mbytes o tamanho da memoria
}

/*********************************** funcao que retorna tamanha do disco **************************************/

float infoDisk() {
	DWORD sec, byt, free_c, total_c;
	float total;
	GetDiskFreeSpace("C:", &sec, &byt, &free_c, &total_c);
	total = (float) (total_c*sec*byt);
	//total = (float(total_c)*float(sec)*float(byt));
	return (total / (1024*1024)); //retorna em Kbytes o tamanho do Hd
}


/*********************funcao que retorna a string do precessador *********************************/

char* infoProcessador(char *buffer) {

	DWORD tam = 255;
	DWORD tipo = 0;
	HKEY hkey;

	//int* p = new int;
	//int* p = malloc(sizeof(*p));

	//int* a = new int[12];
	//int* a = malloc(12*sizeof(*a));

	//UCHAR *btmp = new UCHAR[tam];
	UCHAR *btmp = malloc(tam*sizeof(btmp));
	UCHAR *ptr = btmp;
	RegOpenKey(HKEY_LOCAL_MACHINE, KEY_PROCESSADOR, &hkey); // abre o registro para pegar a stringo do processador
	RegQueryValueEx(hkey, "ProcessorNameString", 0, &tipo, btmp, &tam); //pega a string do processador na variavel do registro ProcessorNameString
	RegCloseKey(hkey);
	while (*(++btmp) == ' ');
	strcpy(buffer, (char*)btmp);
	free (ptr);
	return buffer; // retorna string do processador
}

/*****************************funcao que retorna a quantidade de memoria de video instalada ******************/
int infoVideo() {

	DWORD tam = 255;
	DWORD tipo = 0;
	DWORD resultado = 0;
	HKEY hkey;
	UCHAR btmp[255], buffer[255] = { 0 };
	RegOpenKey(HKEY_LOCAL_MACHINE, KEY_VIDEO, &hkey);
	RegQueryValueEx(hkey, "\\Device\\Video0", 0, &tipo, btmp, &tam);
	char *ptr = (char*)btmp + strlen("\\Registry\\Machine\\");
	RegOpenKey(HKEY_LOCAL_MACHINE, ptr, &hkey); // abre o registro para pegar o tamanho de memmoria de video
	RegQueryValueEx(hkey, "HardwareInformation.MemorySize", 0, &tipo, buffer, &tam); //pega o tamanho de video na variavel HardwareInformation.MemorySize
	RegCloseKey(hkey);
	memcpy((void*)&resultado, (void*)buffer, sizeof(DWORD)); //copia o valor de resultado para buffer
	return resultado / (1024 * 1024); //retorna o tamanho da memoria de video em MB
}

/***********************funcao que retorna a string da placa de video *************************************/
char* infoVideoString(char* retBuf) {

	DWORD tam = 255;
	DWORD tipo = 0;
	DWORD resultado = 0;
	HKEY hkey;
	UCHAR btmp[255], buffer[255] = { 0 };
	RegOpenKey(HKEY_LOCAL_MACHINE, KEY_VIDEO, &hkey); // abreo o rgistro para pegar informa�oes da stringo do video
	RegQueryValueEx(hkey, "\\Device\\Video0", 0, &tipo, btmp, &tam);
	char *ptr = (char*)btmp + strlen("\\Registry\\Machine\\");
	RegOpenKey(HKEY_LOCAL_MACHINE, ptr, &hkey);
	RegQueryValueEx(hkey, "Device Description", 0, &tipo, buffer, &tam); //pega a string da placa de video na variavel Device Description
	memcpy((void*)retBuf, (void*)buffer, strlen((char*)buffer)); //faz a copia das informa�oes do registro para buffer
	RegCloseKey(hkey);
	return retBuf;  //retorna a string da placa de video
}



/******************************** funcao que retorna o clock do processador **********************************************/

long getClockProcessador()
{
	DWORD tam = 255;
	DWORD tipo = 0;
	HKEY hkey;
	long clock;

	//int* p = new int;
	//int* p = malloc(sizeof(*p));

	//int* a = new int[12];
	//int* a = malloc(12*sizeof(*a));

	//UCHAR *btmp = new UCHAR[tam];
	UCHAR *btmp = malloc(tam*sizeof(*btmp));
	RegOpenKey(HKEY_LOCAL_MACHINE, KEY_PROCESSADOR, &hkey); //abre o registro para pegar informa��oes do clock
	RegQueryValueEx(hkey, "~MHz", 0, &tipo, btmp, &tam); //pega o valor do clock do processador na variavel do registro ~MHz
	RegCloseKey(hkey);
	memcpy((void*)&clock, (void*)btmp, sizeof(long)); //copia o valor do clock encontrado no registro para buffer
	free (btmp);
	return clock;  //retorna o clock do processador em MHz
}

/*char* infoBrand(char *buffer) {

	//TO-DO
}*/

#endif /* INFO_H_ */
