.model small
.stack
.code

; move o valor 2h para o registrador ah
mov ah, 2h

; move o valor 2bh para o registrador dl (simbolo de mais no ASCII)
mov dl, 2bh

;interrupcao 21h
int 21h

; move o valor 4ch para o registrador ah. A funcao 4c vai para o SO
mov ah, 4ch

; interrupao 21
int 21h

; finaliza o programa
end
