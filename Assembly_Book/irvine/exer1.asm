; use para colocar comentario no programa assembler
.MODEL SMALL
; modelo de memoria
.STACK
; espaco de memoria para instrucoes do programa na pilha
.CODE
; as linhas seguintes sao instrucoes de programa
mov ah, 1h
; move o valor 1h para o registrador ah
mov cx, 07h
; move o valor 07h para o registrador cx
int 10h
; interrupcao 10h
mov ah, 4ch
; move o valor 4ch para o registrador ah
int 21h
; interrupcao 21h
END
; finaliza o codigo do programa
