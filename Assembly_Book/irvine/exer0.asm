; //Description: Apenas um Olá mundo
;
; //Revision date : 11 de agosto de 2014

INCLUDE Irvine32.inc
.data
    myMessage BYTE "Meu primeiro Assembly", 0dh, 0ah, 0
.code
    main PROC
        call Clrscr

        mov edx, OFFSET myMessage
        call WriteString

        exit
    main ENDP

END main
