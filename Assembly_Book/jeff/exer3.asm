;   exer3.asm
;   autor: Anderson Moreira
;   data: 11 de setembro de 2013
;   resumo: programa que imprime a letra A na tela 10 vezes com uma cor e fundo diferentes do usual
;   tabela RGB
;   Red     Green       Blue        Decimal     Cor
;   ------------------------------------------------
;   0       0           1           1           azul
;   0       1           0           2           verde
;   0       1           1           3           azul esverdeado
;   1       0           0           4           vermelho
;   1       0           1           5           purpura
;   1       1           0           6           marrom
;   1       1           1           7           branco
;   0       0           0           0           preto

.MODEL SMALL
.STACK
.DATA
.CODE
main:
    ;inicio da rotina 
    ;escreve um caracter na tela (inicio)
    mov AH, 09              ;imprime um caractere
    mov AL, 'A'             ;escreve o caracter que deve aparecer na tela
    mov BH, 00              ;p?gina n?mero 00
    mov BL, 14H             ;cor no atributo RGB. Hexadecimal 14H (Vermelho com azul) 1 = azul 4 = vermelho
    mov CX, 10D             ;repeti??es
    int 10H                 ;chama servi?o de v?deo
    ;escreve um caracter na tela (fim)
    ;fim da rotina
    
    mov AH, 4CH
    int 21H
end main
