;   exer6.asm
;   autor: Anderson Moreira
;   data: set de 2013
;   resumo: programa que pinta uma por??o da tela

.MODEL SMALL
.STACK
.DATA
    strMsg DB "I love TADS$"
.CODE

main:
    mov AX,@DATA
    mov DS,AX
    mov ES,AX
    
    mov AH,00H
    mov AL, 03H
    int 10H
    
    mov AX,0600H
    mov BH,46H
    
    mov CH,0D
    mov CL,0D
    mov DH,2D
    mov DL,10D
    int 10H
    
    mov AH,09H
    lea DX,strMsg
    
    int 21H
    
    mov AH,4CH
    int 21H
end main
    
