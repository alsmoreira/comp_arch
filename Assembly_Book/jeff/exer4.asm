;   exer4.asm
;   autor: Anderson Moreira
;   data: 19 de setembro de 2013
;   resumo: programa que imprime duas vari?veis no formato string

.MODEL SMALL
.STACK
.DATA
    ;declara a string no segmento de DADOS
    strMsg1 DB "Forma simples de aprender$"
    strMsg2 DB 0AH, "Linguagem Assembly no IFPE$"   ;0AH significa retorno (Enter) / Next Line 
.CODE
main:
    ;inicializa SEG&OFF para a string (inicio)
    mov AX, @DATA   ;inicializa DS$ES (Segmento e Deslocamento (Offset))
    mov DS, AX
    mov ES, AX
    ;inicializa SEG&OFF para a string (fim)
    
    ;mostra a string de uma vari?vel (inicio)
    mov AH, 09H     ;requisita a string para ser exibida
    lea DX, strMsg1 ;Load Effective Address da string (ambos segmento e deslocamento)
    int 21H         ;retorna ao DOS
    ;mostra a string de uma vari?vel (fim)
    
    ;mostra a string de uma vari?vel (inicio)
    mov AH, 09H     ;requisita a string para ser exibida
    lea DX, strMsg2 ;Load Effective Address da string (ambos segmento e deslocamento)
    int 21H         ;retorna ao DOS
    ;mostra a string de uma vari?vel (fim)
    
    mov AH, 4CH
    int 21H
end main
