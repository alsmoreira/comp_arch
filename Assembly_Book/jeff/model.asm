;   model.asm
;   autor: Anderson Moreira
;   data: 11 de setembro de 2013
;   resumo: modelo de arquivo em assembly. Este arquivo ? utlizado como um template usado na maioria dos programas
;           de 16-bits.

.MODEL SMALL
.STACK
.DATA
    <var_nome1> DB <valor>
.CODE
main:
    mov DX, OFFSET Mensagem
    mov AX, SEG Mensagem
    mov DS,AX
    
    
    mov AH, 4CH
    int 21H
end main
