;   exer2.asm
;   autor: Anderson Moreira
;   data: 11 de setembro de 2013
;   resumo: programa que imprime a letra A na tela

.MODEL SMALL
.STACK
.DATA
.CODE
main:
    ;inicio da rotina 
    ;escreve um caracter na tela (inicio)
    mov AH, 02H             ;imprime um caractere
    mov DL, 'A'
    int 21H
    ;escreve um caracter na tela (fim)
    ;fim da rotina
    
    mov AH, 4CH
    int 21H
end main
