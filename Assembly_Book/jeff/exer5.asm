;   exer5.asm
;   autor: Anderson Moreira
;   data: set de 2013
;   resumo: programa que limpa a tela - similar ao clrscr() do C

.MODEL SMALL
.STACK
.DATA
    strMsg DB "Amo programar em Assembly$"      ;declara uma string no segmento DATA
.CODE
main:
    ;inicializa SEG&&OFF para mostrar a string
    mov AX, @DATA
    mov DS, AX
    mov ES, AX
    
    ;limpa a tela
    mov AH, 00H     ;inicializa o modo de vudeo
    mov AL, 03H     ;Video Mode 03 = 80x25
    int 10H
    
    ;mostra a string armazenada na vari?vel
    mov AH, 09H     ;requisita uma string para mostrar
    lea DX, strMsg  ;Load Effective Address of the string
    int 21H
    
    mov AH, 4CH
    int 21H
end main