;   exer1.asm
;   autor: Anderson Moreira
;   data: 18 de setembro de 2013
;   resumo: imprime na tela um ola mundo

.model small
.stack
.data
     Mensagem DB "Ola mundo!$"
.code

main:                           ;isso ? um coment?rio
     mov DX, OFFSET Mensagem    ;offset da mensagem armazendo em DX
     mov AX, SEG Mensagem       ;segmento da mensagem est? em AX
     mov DS, AX                 ;DS:DX aponta para uma string

     mov AH, 9D                 ;fun??o 9D mostra a string
     INT 21H                    ;chama o servi?o do DOS

     mov AH, 4CH                ;c?digo para terminar o programa
     int 21H                    ;chama o servi?o do DOS
end main                        ;finaliza o programa principal e aguarda a parada 
