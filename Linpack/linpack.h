/*
 * linpack.h
 *
 *  Created on: 13/05/2011
 *      Author: Anderson Moreira
 *      e-mail: alsm4@cin.ufpe.br
 *
 *  O rel�gio de um PC � atualizado em torno de 18 vezes por segundo com uma resolu��o de
 *  0,05 a 0,06 segundos que � similar ao tempo pego pela fun��o principal de consumo do
 *  tempo 'dgefa' em um Pentium 100MHz. Esta rotina trabalha com ponto flutuante de 64 bits.
 *  Ele cont�m dois conjuntos de rotinas: um para decomposi��o de matrizes e outro para resolver
 *  o sistema de equa��es lineares resultantes da decomposi��o. Os resultados dos testes s�o
 *  reportados em MFLOPS (Millions of Floating Point Operations per Second), GFLOPS (Billions
 *  of Floating Point Operations per Second) ou TFLOPS (Trillions of Floating Point Operations
 *  per Second). Sua aplica��o � visivelmente em m�quinas que utilizam softwares para c�lculos
 *  cient�ficos e de engenharia, visto que as opera��es mais utilizadas nestes tipos de aplica��es
 *  s�o em ponto-flutuante.
 */

#ifndef NULL
#define NULL   ((void *) 0)
#endif

#ifdef SP
#define REAL float
#define ZERO 0.0
#define ONE 1.0
#define PREC "Single "
#endif

#ifdef DP
#define REAL double
#define ZERO 0.0e0
#define ONE 1.0e0
#define PREC "Double "
#endif

#ifdef ROLL
#define ROLLING "Rolled "
#endif

#ifdef UNROLL
#define ROLLING "Unrolled "
#endif


#define NTIMES 10

#include <stdio.h>
#include <math.h>
//#include <conio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>  /* for following time functions only */

/*Inicio de bibliotecas para identificação de hardware */
#include <windows.h>
#include <psapi.h>
#include <tchar.h>
#pragma comment(lib, "user64.lib")

#define KEY_PROCESSADOR "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0"
#define KEY_VIDEO "HARDWARE\\DEVICEMAP\\VIDEO"
/*fim*/

static REAL atime[9][15];
static char this_month;
static int this_year;

void print_time (int row);
void matgen(REAL a[], int lda, int n, REAL b[], REAL *norma);
void dgefa(REAL a[], int lda, int n, int ipvt[], int *info);
void dgesl(REAL a[],int lda,int n,int ipvt[],REAL b[],int job);
void dmxpy(int n1, REAL y[], int n2, int ldm, REAL x[], REAL m[]);
void daxpy(int n, REAL da, REAL dx[], int incx, REAL dy[], int incy);
REAL epslon(REAL x);
int idamax(int n, REAL dx[], int incx);
void dscal(int n, REAL da, REAL dx[], int incx);
REAL ddot(int n, REAL dx[], int incx, REAL dy[], int incy);

#ifndef LINPACH_H_
#define LINPACH_H_


#endif /* LINPACH_H_ */
