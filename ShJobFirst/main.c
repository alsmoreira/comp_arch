/*
 ===========================================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 18 de mar de 2013
 Copyright   : 
 Description : 
 	 	 	 	 Shortest job next (SJN), also known as Shortest Job First (SJF)
 	 	 	 	 or Shortest Process Next (SPN), is a scheduling policy that
 	 	 	 	 selects the waiting process with the smallest execution time to
 	 	 	 	 execute next. SJN is a non-preemptive algorithm. Shortest
 	 	 	 	 remaining time is a preemptive variant of SJN.

				 Shortest job next is advantageous because of its simplicity and
				 because it minimizes the average amount of time each process has
				 to wait until its execution is complete. However, it has the
				 potential forprocess starvation for processes which will require
				 a long time to complete if short processes are continually added.
				 Highest response ratio next is similar but provides a solution
				 to this problem using a technique called aging.

				 Another disadvantage of using shortest job next is that the total
				 execution time of a job must be known before execution. While it
				 is not possible to perfectly predict execution time, several methods
				 can be used to estimate the execution time for a job, such as a
				 weighted average of previous execution times.

				 Shortest job next can be effectively used with interactive processes
				 which generally follow a pattern of alternating between waiting for
				 a command and executing it. If the execution burst of a process is
				 regarded as a separate �job�, past behaviour can indicate which process
				 to run next, based on an estimate of its running time.

				 Shortest job next is used in specialized environments where accurate
				 estimates of running time are available.
				 In shortest job first scheduling algorithm, the processor selects the
				 waiting process with the smallest execution time to execute next.
 ===========================================================================================
 */

#include "stdio.h"
#include "time.h"

void SJN(int n)
{
    int bt[20], p[20], wt[20], tat[20];
    int i, j, total = 0, pos, temp;
    float avg_wt, avg_tat;

    printf("\nEnter Burst Time:\n");

    for(i=0; i<n; i++)
    {
        printf("p%d:", i+1);
        scanf("%d", &bt[i]);
        p[i] = i+1; //contains process number
    }

    //sorting burst time in ascending order using selection sort
    for(i=0; i<n; i++)
    {
        pos=i;
        for(j=i+1; j<n; j++)
        {
            if(bt[j] < bt[pos])
                pos=j;
        }

        temp=bt[i];
        bt[i]=bt[pos];
        bt[pos]=temp;

        temp=p[i];
        p[i]=p[pos];
        p[pos]=temp;
    }

    wt[0] = 0; //waiting time for first process will be zero

    //calculate waiting time
    for(i=1; i<n; i++)
    {
        wt[i]=0;
        for(j=0; j<i; j++)
            wt[i]+=bt[j];

        total+=wt[i];
    }

    avg_wt = (float)total / n; //average waiting time
    total=0;

    printf("\nProcess\t    Burst Time    \tWaiting Time\tTurnaround Time");
    for(i=0;i<n;i++)
    {
        tat[i]=bt[i]+wt[i]; //calculate turnaround time
        total+=tat[i];
        printf("\np%d\t\t  %d\t\t    %d\t\t\t%d", p[i], bt[i], wt[i], tat[i]);
    }

    avg_tat = (float)total / n; //average turnaround time

    printf("\n\nAverage Waiting Time = %f", avg_wt);
    printf("\nAverage Turnaround Time = %f\n", avg_tat);
}

int main(int argc, char *argv[])
{
	int num;
	double tm;
	clock_t t1, t2;

    printf("Enter number of process: ");
    scanf("%d", &num);

    t1 = clock();
    SJN(num);
    t2 = clock();

    tm = (1000.0 * ((double)t2 - (double)t1) / (double)CLOCKS_PER_SEC);

    printf ("Execution time = %.3e mSecs\n\n", tm);

    return 0;
}
