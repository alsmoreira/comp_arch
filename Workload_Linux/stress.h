#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <string.h>

typedef unsigned int uintmax_t;
typedef int dev_t;

#include <fsusage.h>
#include <mountlist.h>

#define true 1
#define false 0

typedef struct _meminfo{
	int total;
	int free;
} meminfo;



int stress();
/* ***************************** Funcoes do teste de HD **************************************** */

int testaHd();
int show_disk (char *point);


/* **************************** Funcoes do teste de Memoria ************************************* */

void* VarreMemoria(void *);
void memset2(unsigned char* memoriaTeste, int valor ,unsigned long tam);
meminfo * freemem(); // Pega informaçoe sobre a memoria:

/* ****************************** Fim Funcoes Teste de Memoria ********************************** */

/* ***************************** Funcoes do calculo da serie de Potencia  *********************** */

long double fatorial(int n); //Fatorial Iterativo
long double fatorial2(int n); //Fatorial Recursivo
double pow2(double x, int y); //Funcao de potencia
void* serieDePotencia(void*); //Calcula a serie de potencia

/* ****************    Fim das Funcoes do calculo da serie de Potencia ****************** */





