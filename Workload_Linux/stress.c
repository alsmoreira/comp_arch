//#include "RealizaStress.h"
#include "stress.h"
/**************************************** Teste de Memoria *********************************************** */
void* VarreMemoria(void * ptr){
	while(1) {
		volatile unsigned char* memoriaTeste = NULL;
		unsigned long i;
		unsigned char j = 0x01;
		meminfo *mi = freemem();

		/*Verifica de ouve erro na aloca��o de memoria no freemem*/
		if(mi == NULL)
			continue;

		unsigned long  tam = mi->free;
		free(mi);
		tam -= (unsigned long)(4.4*1024*1024);
		if(tam <= 0)
			continue;
		

		/*Aloca toda a memoria livre*/
		memoriaTeste = malloc(sizeof(volatile unsigned char)*tam);

		/*Verifica se ouve erro na aloca��o de memoria */
		if(memoriaTeste == NULL)
			continue;

		while(j != 0){
			memset2(memoriaTeste,0,sizeof(unsigned char)*tam);
			for(i=0; i < tam; i++){
				/* Verifica se continua com zero a memoria a frente. */
				if(memoriaTeste[i] != 0);
				
				/* Faz uma copia na memoria de teste */
				memoriaTeste[i] = j;

				/* Verifica se a memoria est� com o valor gravado */
				if(memoriaTeste[i] != j);
			}	
			j <<= 1;
		}
		free(memoriaTeste);
	}


}

/*Varre a memoria substituindo-a por um "valor" */
void memset2(unsigned char* memoriaTeste, int valor ,unsigned long tam){
	while(tam--)
		*(memoriaTeste++) = valor;
}

/* Pega informa�oe sobre a memoria:
* - Livre
* - Total
*/
meminfo * freemem(){
	FILE *arq = fopen("/proc/meminfo","r");
	meminfo *mi = malloc(sizeof(meminfo));

	/*Verifica se conseguio alocar memoria*/
	if(mi == NULL){
		fclose(mi);
		return NULL;
	}
	char linha[500] = {0}, *aux;
	int free = 0, total = 0, usada = 0;

	fread(linha,500,1,arq);
	aux = linha + 65;
	sscanf(aux,"%i %i %i",&total,&usada,&free);
	mi->total = total;
	mi->free = free;

	fclose(arq);
	return mi;
}

/* **************************************Fim Teste de Memoria********************************************* */



/* ********************************** Inicio Da Serie de Potencia **************************************** */
//Fatoria Interativo
long double fatorial(int n){
	long double x = 1;
	volatile int j;
	if(n==1 || n ==0)
		return x;
	for(j = n; j > 0 ; --j)
		x *= j;
	return x;
}
//Fatoria Recursivo
long double fatorial2(int n){
	if(n==1 || n ==0)
		return 1;
	return n * fatorial2(n-1);
}

double pow2(double x, int y){
	volatile int i;
	for(i=0; i < y; ++i)
		x *= x;
}

void* serieDePotencia(void* x){
	long double qnt = 10, soma = 0, i, n ;
	while(1){
	soma = 0;
	for (n=0; n < qnt; ++n)
		soma += pow2(sin(n),n) / fatorial(n);
	}


}

/* *********************************** FIM da Serie de Potencia ******************************************* */


unsigned int aux = 0;
char nomeArquivo[255] = {0};
/* ************************************* Inicio do teste de Hard Disk ************************************* */
void* hd(void* ptr){

	while(1){
	unsigned char val = 0xFF;
	unsigned char val2 = 0x00;
	register int i;
	unsigned int tam = 0, cont = 0;
	int livre = 0, pc = 30;
	FILE *arq;
	int limite = 1000000;

	memset(nomeArquivo,0,255);
	aux = 0;
        if((livre = show_disk("/")) == -1)
		return false;
	

	/*Verifica se n�o ouve erro de aloca��o de memoria no show_disk */
        if(livre == -2)
		continue;

	tam = ((pc/100.)*livre);
	if(tam > limite){
		aux = cont = tam / limite;
		tam -= cont*limite;
		limite += tam / cont;
	}
	else{
		limite = tam;
		cont = 1;
	}

	while(cont--){
		sprintf(nomeArquivo,"teste%i.data",cont);
		arq = fopen(nomeArquivo,"w+");
		if(!arq)
			continue;
		
		for(i=0;i<limite*1024;i++)
		{
			fwrite(&val,1,1,arq);
		}
		rewind(arq);
		for(i=0;i<limite*1024;i++)
		{
			fread(&val2,1,1,arq);
			if(val2 != val)
				continue;
			
		}
	}
	fclose(arq);
	while(aux--){
		sprintf(nomeArquivo,"teste%i.data",aux);
		remove(nomeArquivo);
	}

	}

	return true;
}

int show_disk (char *point)
{
	static struct mount_entry *mount_list = 0;

	unsigned char *ptr = (unsigned char*)malloc(2*sizeof(struct fs_usage));

	/* caso nao consiga alocar memoria */
	if(ptr == NULL)
		return -2;

	struct fs_usage *fsu = (struct fs_usage*)ptr;
	struct mount_entry *me = 0, *me_falso = 0;
	int achou = false;

	mount_list = read_filesystem_list(0);


	for (me = mount_list; me; me = me->me_next)
	{
		if (STREQ (me->me_mountdir, point) && !STREQ (me->me_type, "lofs"))
		{
			if(!me->me_dummy){
				achou = true;
				break;
			}
			me_falso = me;
		}
	}
	if(!achou && me_falso)
		me = me_falso;
	else
		return -1;

	if(get_fs_usage( "/" ,me->me_devname, fsu))
	{
		return -1;
	}

	free(me);

//	printf("Espaco Total: %i \n",(fsu->fsu_blocksize/1024)*(fsu->fsu_blocks));
//	printf("Espaco Livre Total: %i \n",(fsu->fsu_blocksize/1024)*fsu->fsu_bavail);

	return (fsu->fsu_blocksize/1024)*fsu->fsu_bavail;
}

/* ************************************** Fim do teste de Hard Disk *************************************** */
/*
JNIEXPORT jboolean JNICALL Java_RealizaStress_RealizaStress
  (JNIEnv *ptr, jobject jthis){
  
if(stress(1800) == 0)  
	return true;
return false;
  
}
*/  
int main(){
if (stress() == 0)
	return 0;
return -1;
}
  
int stress(){

pthread_t threadSerie, threadMemoria, threadBurn, threadHd;
int iretSerie, iretMemoria, iretBurn, iretHd;

iretMemoria = pthread_create(&threadMemoria, NULL, (void*)&VarreMemoria, NULL);
iretSerie = pthread_create(&threadSerie, NULL, (void*)&serieDePotencia, NULL);
iretHd = pthread_create(&threadHd, NULL, (void*)&hd, NULL);

sleep(120); //30 minutos

while(aux--){
	sprintf(nomeArquivo,"teste%i.data",aux);
	remove(nomeArquivo);
}

return 0;
}
