/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 11 de mai de 2017
 Copyright   : 
 Description : Peneira de Erat�stenes

 	 	 	 	 � um benchmark padr�o utilizado para determinar a
 	 	 	 	 velocidade relativa de diferentes computadores ou,
 	 	 	 	 neste caso, a efici�ncia do c�digo gerado para o mesmo
 	 	 	 	 computador por diferentes compiladores. O algoritmo
 	 	 	 	 da peneira foi desenvolvido na Gr�cia antiga e � um de
 	 	 	 	 uma s�rie de m�todos usados para encontrar n�meros primos.

 	 	 	 	 A peneira trabalha com um processo de elimina��o usando
 	 	 	 	 um array que come�a com 2 e mantem todos os n�mero
 	 	 	 	 na posi��o. O processo �:

 	 	 	 	 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23

 	 	 	 	 Come�ar ap�s 2, eliminar todos os m�ltiplos de 2.

 	 	 	 	 Come�ar ap�s 3, eliminar todos os m�ltiplos de 3.

 	 	 	 	 Come�ar ap�s 5, eliminar todos os m�ltiplos de 5.

 	 	 	 	 Continue at� o pr�ximo n�mero restante ser maior do que a
 	 	 	 	 raiz quadrada do maior n�mero de s�rie original.
 	 	 	 	 Neste caso o pr�ximo n�mero, 7, � maior do que a raiz
 	 	 	 	 quadrada de 25, de modo que o processo p�ra.
 	 	 	 	 Os outros n�meros s�o todos os primos.

 	 	 	 	 2 3 5 7 11 13 17 19 23


 ============================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LIMITE 1000000 /* tamanho do array de inteiros */

void sieve ()
{
	unsigned long long int i,j;
	int *primos;
	int z = 1;

	/*
	 * A fun��o malloc() serve para alocar mem�ria,
	 * ou seja, reserva um numero especifico de memoria,
	 * dependendo do tipo do ponteiro que chama a fun��o.
	 */

	primos = malloc(LIMITE*sizeof(int));

	for (i=2;i<LIMITE;i++)
	{
		primos[i]=1;
	}

	/* peneirar os n�o primos */
	for (i=2;i<LIMITE;i++)
	{
		if (primos[i])
	    {
			for (j=i;i*j<LIMITE;j++)
	        {
				primos[i*j]=0;
	        }
        }
    }


	for (i=2;i<LIMITE;i++)
    {
    	if (primos[i])
	    {
    		printf("%d� primo = %lld \n",z++,i);
	    }
	}
    free(primos);
}

int main()
{
	clock_t t1, t2;
	double tm;
	printf ("Sieve benchmark,  Versao 1.1\n");

	t1 = clock ();
    sieve();
    t2 = clock ();

    tm = (1000.0 * ((double)t2 - (double)t1) / (double)CLOCKS_PER_SEC);

    printf ("Tempo necessario = %.3e mSecs\n\n", tm);

    return 0;
}
