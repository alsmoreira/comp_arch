/*
 ============================================================================
 Name        : stress.h
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 24/08/2011
 Copyright   : 
 Description : 
 ============================================================================
 */

#ifndef STRESS_H_
#define STRESS_H_

#include <stdio.h>
#include <math.h>
#include <process.h>
#include <string.h>
#include <windows.h>

#define true 1
#define false 0

/* teste de stress */
int stress(unsigned long tempo);

/* testa o HD */
int testaHd();

/* teste de mem�ria */
void VarreMemoria(void*);
void memset2(volatile unsigned char* memoriaTeste, int valor ,unsigned long tam);

/* c�lculo de pot�ncia */
long double fatorial(int n); //Fatorial Interativo
void serieDePotencia(void*); //Calcula a serie de potencia

/* executa stress */
void run();

#endif /* STRESS_H_ */
