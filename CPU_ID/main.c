/*
 *  File:       main.c (Solution: CPU_ID)
 *
 *  Date:       24/04/2017
 *
 *  Author:     Anderson L. S. Moreira
 *
 *  gprof CPU_ID.exe gmon.out > analise.txt
 */

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <psapi.h>

#pragma comment(lib, "user64.lib")

int main(int argc, char * argv[])
{

	SYSTEM_INFO siSysInfo;
	MEMORYSTATUSEX memoryStatus;
	PERFORMANCE_INFORMATION performanceInformation;

	// Copia a informa��o para a estrutura
	// TO-DO: Verificar problema no
	//         GetPerformanceInfo(&performanceInformation, sizeof(performanceInformation));

	GetSystemInfo(&siSysInfo);
	GlobalMemoryStatus(&memoryStatus);
	memoryStatus.dwLength = sizeof(memoryStatus);


	// Mostra o conte�do da estrutura, apesar que o 'wmic cpu' � mais completo

	printf("Informacao do hardware: \n");
	printf("  OEM ID: %lu\n", siSysInfo.dwOemId);
	printf("  Numero de processadores logicos: %lu\n", siSysInfo.dwNumberOfProcessors);
	printf("  Tamanho da pagina: %lu\n", siSysInfo.dwPageSize);
	printf("  Tipo de processador: %u\n", siSysInfo.dwProcessorType);
	printf("  Endereco minimo para aplicacao: %lx\n", siSysInfo.lpMinimumApplicationAddress);
	printf("  Endereco maximo para aplicacao: %lx\n", siSysInfo.lpMaximumApplicationAddress);
	printf("  Mascara de processador ativa: %lu\n", siSysInfo.dwActiveProcessorMask);
	printf("  Arquitetura do processador: %u\n", siSysInfo.wProcessorArchitecture);
	printf("  Nivel do processador: %u\n", siSysInfo.wProcessorLevel);
	printf("  Revisao do processador: %u\n", siSysInfo.wProcessorRevision);

	printf("Informacao de memoria: \n");
	printf("  Expansao de memoria: %lu\n", memoryStatus.dwLength);
	printf("  Memoria carregada: %lu%%\n", memoryStatus.dwMemoryLoad);
	printf("  Mempria fisica total: %u Mbytes\n", memoryStatus.ullTotalPhys / (1024 * 1024));
	printf("  Memoria fisica disponivel: %u Mbytes\n", memoryStatus.ullAvailPhys / (1024 * 1024));
	printf("  Memoria total do arquivo de paginacao: %u Mbytes\n",	memoryStatus.ullTotalPageFile / (1024 * 1024));
	printf("  Memoria disponivel do arquivo de paginacao: %u Mbytes\n",	memoryStatus.ullAvailPageFile / (1024 * 1024));
	printf("  Memoria virtual total: %u Mbytes\n", memoryStatus.ullTotalVirtual / (1024 * 1024));
	printf("  Memoria virtual disponivel: %u Mbytes\n", memoryStatus.ullAvailVirtual / (1024 * 1024));

	printf("Informacao da cache: \n");
	printf("  Cache do sistema: %lu Mbytes\n", performanceInformation.SystemCache / (1024 * 1024));

	system("wmic cpu > temp.dat");

	exit(0);
}


/* ----------------------------------------------------------------------
 * Demo2: GetSystemInfo, GetPerformanceInfo, GlobalMemoryStatusEx,
 *        GetProcessMemoryInfo. Observar o resultado produzido por este
 *        programa com a execu��o da demo1.

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <windows.h>
#include <psapi.h>

void printSystemInfo(LPSYSTEM_INFO psi) {
	printf("SystemInfo.wProcessorArchitecture            = %10u\n", psi->wProcessorArchitecture);
	printf("SystemInfo.dwPageSize                        = %10u\n", psi->dwPageSize);
	printf("SystemInfo.lpMinimumApplicationAddress       = %10u\n", psi->lpMinimumApplicationAddress);
	printf("SystemInfo.lpMaximumApplicationAddress       = %10u\n", psi->lpMaximumApplicationAddress);
	printf("SystemInfo.dwActiveProcessorMask             = %10u\n", psi->dwActiveProcessorMask);
	printf("SystemInfo.dwNumberOfProcessors              = %10u\n", psi->dwNumberOfProcessors);
	printf("SystemInfo.dwProcessorType                   = %10u\n", psi->dwProcessorType);
	printf("SystemInfo.dwAllocationGranularity           = %10u\n", psi->dwAllocationGranularity);
	printf("SystemInfo.wProcessorLevel                   = %10u\n", psi->wProcessorLevel);
	printf("SystemInfo.wProcessorRevision                = %10u\n", psi->wProcessorRevision);
}
void printGlobalMemoryStatus(LPMEMORYSTATUSEX pms) {
	printf("GlobalMemoryStatusEx.dwLength                = %10u\n",			pms->dwLength);
	printf("GlobalMemoryStatusEx.dwMemoryLoad            = %10u%%\n",		pms->dwMemoryLoad);
	printf("GlobalMemoryStatusEx.ullTotalPhys            = %10u Kbytes\n",	pms->ullTotalPhys / 1024);
	printf("GlobalMemoryStatusEx.ullAvailPhys            = %10u Kbytes\n",	pms->ullAvailPhys / 1024);
	printf("GlobalMemoryStatusEx.ullTotalPageFile        = %10u Kbytes\n",	pms->ullTotalPageFile / 1024);
	printf("GlobalMemoryStatusEx.ullAvailPageFile        = %10u Kbytes\n",	pms->ullAvailPageFile / 1024);
	printf("GlobalMemoryStatusEx.ullTotalVirtual         = %10u Kbytes\n",	pms->ullTotalVirtual / 1024);
	printf("GlobalMemoryStatusEx.ullAvailVirtual         = %10u Kbytes\n",	pms->ullAvailVirtual / 1024);
}
void printProcessMemoryInfo(PPROCESS_MEMORY_COUNTERS_EX pmc) {
    printf( "ProcessMemoryCountersEx.cb                         = %10u bytes\n", pmc->cb );
    printf( "ProcessMemoryCountersEx.PageFaultCount             = %10u\n",		 pmc->PageFaultCount );
    printf( "ProcessMemoryCountersEx.PeakWorkingSetSize         = %10u Kbytes\n", pmc->PeakWorkingSetSize / 1024);
    printf( "ProcessMemoryCountersEx.WorkingSetSize             = %10u Kbytes\n", pmc->WorkingSetSize / 1024);
    printf( "ProcessMemoryCountersEx.QuotaPeakPagedPoolUsage    = %10u Kbytes\n", pmc->QuotaPeakPagedPoolUsage / 1024);
    printf( "ProcessMemoryCountersEx.QuotaPagedPoolUsage        = %10u Kbytes\n", pmc->QuotaPagedPoolUsage / 1024 );
    printf( "ProcessMemoryCountersEx.QuotaPeakNonPagedPoolUsage = %10u Kbytes\n", pmc->QuotaPeakNonPagedPoolUsage / 1024 );
    printf( "ProcessMemoryCountersEx.QuotaNonPagedPoolUsage     = %10u Kbytes\n", pmc->QuotaNonPagedPoolUsage / 1024 );
    printf( "ProcessMemoryCountersEx.PagefileUsage              = %10u Kbytes\n", pmc->PagefileUsage / 1024 );
    printf( "ProcessMemoryCountersEx.PeakPagefileUsage          = %10u Kbytes\n", pmc->PeakPagefileUsage / 1024 );
    printf( "ProcessMemoryCountersEx.PrivateUsage               = %10u Kbytes\n", pmc->PrivateUsage / 1024 );
}
void printPerformanceInfo(PPERFORMANCE_INFORMATION pi) {
	printf("PerformanceInfo.CommitTotal         		= %10u KBytes\n",	pi->CommitTotal * pi->PageSize / 1024);
	printf("PerformanceInfo.CommitLimit         		= %10u KBytes\n",	pi->CommitLimit * pi->PageSize / 1024);
	printf("PerformanceInfo.CommitPeak          		= %10u KBytes\n",	pi->CommitPeak * pi->PageSize / 1024);
	printf("PerformanceInfo.PhysicalTotal       		= %10u Kbytes\n",	pi->PhysicalTotal * pi->PageSize / 1024);
	printf("PerformanceInfo.PhysicalAvailable   		= %10u Kbytes\n",	pi->PhysicalAvailable * pi->PageSize / 1024);
	printf("PerformanceInfo.SystemCache         		= %10u Kbytes\n",	pi->SystemCache * pi->PageSize / 1024);
	printf("PerformanceInfo.KernelTotal         		= %10u Kbytes\n",	pi->KernelTotal * pi->PageSize / 1024);
	printf("PerformanceInfo.KernelPaged         		= %10u Kbytes\n",	pi->KernelPaged * pi->PageSize / 1024);
	printf("PerformanceInfo.KernelNonpaged      		= %10u Kbytes\n",	pi->KernelNonpaged * pi->PageSize / 1024);
	printf("PerformanceInfo.PageSize            		= %10u Kbytes\n",	pi->PageSize / 1024);
	printf("PerformanceInfo.HandleCount         		= %10u\n",			pi->HandleCount);
	printf("PerformanceInfo.ProcessCount        		= %10u\n",			pi->ProcessCount);
	printf("PerformanceInfo.ThreadCount         		= %10u\n",			pi->ThreadCount);
}


int main(int argc, char * argv[]) {

	SYSTEM_INFO systemInfo;
	MEMORYSTATUSEX memoryStatus;
	PROCESS_MEMORY_COUNTERS_EX processMemory, lastProcessMemory = {0};
	PERFORMANCE_INFORMATION performanceInformation;
	BOOL res;
	int i, pid;
	HANDLE h;

	GetSystemInfo(&systemInfo);
	printSystemInfo(&systemInfo);
	putchar('\n');

	res = GetPerformanceInfo(&performanceInformation, sizeof(performanceInformation));
	if ( res == FALSE ) {
		fprintf(stderr, "Error calling GetPerformanceInfo(...). GetLastError = %u\n", GetLastError());
		exit(0);
	}
	printPerformanceInfo(&performanceInformation);

	memoryStatus.dwLength = sizeof(memoryStatus);
	res = GlobalMemoryStatusEx(&memoryStatus);
	if ( res == FALSE ) {
		fprintf(stderr, "Error calling GlobalMemoryStatusEx(...). GetLastError = %u\n", GetLastError());
		exit(0);
	}
	printGlobalMemoryStatus(&memoryStatus);

	printf("Insert process PID: ");
	i = scanf("%d", &pid);
	if (i == 0)
		pid = GetCurrentProcessId();

	h = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid); // Falta obter o ID do processo demo1.exe
    if (NULL == h)
        return;

	while(1) {
		res = GetProcessMemoryInfo(h, (PPROCESS_MEMORY_COUNTERS)&processMemory, sizeof(processMemory));
		if ( res == FALSE ) {
			fprintf(stderr, "Error calling GlobalMemoryStatusEx(...). GetLastError = %u\n", GetLastError());
			exit(0);
		}
		if (memcmp(&processMemory, &lastProcessMemory, sizeof(PROCESS_MEMORY_COUNTERS_EX)) != 0) {
			printProcessMemoryInfo(&processMemory);
			lastProcessMemory = processMemory;
		}
		Sleep(1000);
	}

	CloseHandle(h);

	printf("Press any key to finish...");
	getchar();

	return 0;
}
*/
