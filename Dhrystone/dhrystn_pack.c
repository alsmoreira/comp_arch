/*
 *
 *  Name:		"DHRYSTONE" Benchmark Program
 *
 *  Version:    C, Version 2.2
 *
 *  File:       dhrystn_pack.c
 *
 *  Date:       March 3, 1988
 *
 *  Author:     Reinhold P. Weicker
 *
 *  Update:		Anderson L. S. Moreira
 *
 *  Date:		Aug 17, 2012
 *
 *  Update:		Anderson L. S. Moreira
 *
 *  Date:		Feb 21, 2017
 *
 */

#include "dhrystone.h"

#ifndef REG
#define REG
        /* REG � definido como vazio    */
        /* no registrador de vari�veis  */
#endif

extern  int     Int_Glob;
extern  char    Ch_1_Glob;


void Proc_6(Enumeration Enum_Val_Par, Enumeration *Enum_Ref_Par) /* Executa uma vez | Enum_Val_Par == Ident_3, Enum_Ref_Par torna-se Ident_2 */
{
	*Enum_Ref_Par = Enum_Val_Par;
	if (! Func_3 (Enum_Val_Par)) /* n�o executa!!!! */
    *Enum_Ref_Par = Ident_4;
	switch (Enum_Val_Par)
	{
    	case Ident_1:
    		*Enum_Ref_Par = Ident_1;
    		break;
    	case Ident_2:
    		if (Int_Glob > 100) *Enum_Ref_Par = Ident_1;
    		else *Enum_Ref_Par = Ident_4;
    		break;
    	case Ident_3: /* executado */
    		*Enum_Ref_Par = Ident_2;
    		break;
    	case Ident_4: break;
    	case Ident_5:
    		*Enum_Ref_Par = Ident_3;
    		break;
  } //fim switch
}


void Proc_7(One_Fifty Int_1_Par_Val, One_Fifty Int_2_Par_Val, One_Fifty *Int_Par_Ref)
/* executa tr�s vezes
 * primeira vez:    Int_1_Par_Val == 2, Int_2_Par_Val == 3,
 *                  Int_Par_Ref recebe 7
 * segunda vez:     Int_1_Par_Val == 10, Int_2_Par_Val == 5,
 *                  Int_Par_Ref recebe 17
 * terceira vez:    Int_1_Par_Val == 6, Int_2_Par_Val == 10,
 *                  Int_Par_Ref recebe 18
 */
{
	One_Fifty Int_Loc;

	Int_Loc = Int_1_Par_Val + 2;
	*Int_Par_Ref = Int_2_Par_Val + Int_Loc;
}


void Proc_8(Arr_1_Dim Arr_1_Par_Ref, Arr_2_Dim Arr_2_Par_Ref, int Int_1_Par_Val, int Int_2_Par_Val)
/* executa apenas uma vez
 * Int_Par_Val_1 == 3
 * Int_Par_Val_2 == 7
 *
 */
{
	REG One_Fifty Int_Index;
	REG One_Fifty Int_Loc;

	Int_Loc = Int_1_Par_Val + 5;
	Arr_1_Par_Ref [Int_Loc] = Int_2_Par_Val;
	Arr_1_Par_Ref [Int_Loc+1] = Arr_1_Par_Ref [Int_Loc];
	Arr_1_Par_Ref [Int_Loc+30] = Int_Loc;
	for (Int_Index = Int_Loc; Int_Index <= Int_Loc+1; ++Int_Index)
		Arr_2_Par_Ref [Int_Loc] [Int_Index] = Int_Loc;
	Arr_2_Par_Ref [Int_Loc] [Int_Loc-1] += 1;
	Arr_2_Par_Ref [Int_Loc+20] [Int_Loc] = Arr_1_Par_Ref [Int_Loc];
	Int_Glob = 5;
}


Enumeration Func_1(Capital_Letter Ch_1_Par_Val, Capital_Letter Ch_2_Par_Val)
/* executa tr�s vezes
 * primeira vez:    Ch_1_Par_Val == 'H', Ch_2_Par_Val == 'R'
 * segunda vez:     Ch_1_Par_Val == 'A', Ch_2_Par_Val == 'C'
 * terceira vez:    Ch_1_Par_Val == 'B', Ch_2_Par_Val == 'C'
 * */
{
	Capital_Letter Ch_1_Loc;
	Capital_Letter Ch_2_Loc;

	Ch_1_Loc = Ch_1_Par_Val;
	Ch_2_Loc = Ch_1_Loc;
	if (Ch_2_Loc != Ch_2_Par_Val)
	{
   		return (Ident_1);
	}
	else  /* n�o executado */
	{
		Ch_1_Glob = Ch_1_Loc;
		return (Ident_2);
	}
}


Boolean Func_2(Str_30  Str_1_Par_Ref, Str_30  Str_2_Par_Ref)
/* executa uma vez
 * Str_1_Par_Ref == "BENCHMARK DHRYSTONE, 1A STRING"
 * Str_2_Par_Ref == "BENCHMARK DHRYSTONE, 2A STRING"
 */
{
	REG One_Thirty Int_Loc;
	Capital_Letter Ch_Loc;

	Int_Loc = 2;
	while (Int_Loc <= 2) // o loop executa apenas uma vez
		if (Func_1 (Str_1_Par_Ref[Int_Loc], Str_2_Par_Ref[Int_Loc+1]) == Ident_1)
        {
			Ch_Loc = 'A';
			Int_Loc += 1;
        } // if, while
	if (Ch_Loc >= 'W' && Ch_Loc < 'Z')
		Int_Loc = 7;
	if (Ch_Loc == 'R')
		return (true);
	else /* executed */
	{
		if (strcmp (Str_1_Par_Ref, Str_2_Par_Ref) > 0)
		{
			Int_Loc += 7;
			Int_Glob = Int_Loc;
			return (true);
		}
		else //executa
			return (false);
	} /* if Ch_Loc */
}


Boolean Func_3(Enumeration Enum_Par_Val)
/* executa apenas uma vez
 * Enum_Par_Val == Ident_3
 */
{
	Enumeration Enum_Loc;

	Enum_Loc = Enum_Par_Val;
	if (Enum_Loc == Ident_3)
		return (true);
	else
		return (false);
} /* Func_3 */
