/*
 *
 *  Name:		"DHRYSTONE" Benchmark Program
 *
 *  Version:    C, Version 2.2
 *
 *  File:       dhrystn_main.c
 *
 *  Date:       March 3, 1988
 *
 *  Author:     Reinhold P. Weicker
 *
 *  Update:		Anderson L. S. Moreira
 *
 *  Date:		Aug 17, 2012
 *
 *  Update:		Anderson L. S. Moreira
 *
 *  Date:		Feb 21, 2017
 *
 *  To GPROF option: gprof Dhrystone_Final.exe gmon.out > analise.txt
 */

#include "dhrystone.h"

/* Vari�veis Globais: */

Rec_Pointer	Ptr_Glob, Next_Ptr_Glob;
int Int_Glob;
Boolean Bool_Glob;
char Ch_1_Glob, Ch_2_Glob;
int Arr_1_Glob [50];
int Arr_2_Glob [50] [50];

//definidas pelo desenvolvedor
int flag = 1; //vari�vel de controle de entrada
char Reg_Define[] = "Opcao por registrador selecionada.";
char general[9][80] = {" "}; //armazenamento de informa��es da m�quina tenho de fazer via assembler

//c�digo antigo n�o funciona mais
//extern char *malloc ();

Enumeration Func_1 ();
  /* repassa a declara��o necess�ria j� que Enumaration n�o pode ser int */

#ifndef REG
        Boolean Reg = false;
#define REG
        /* REG torna-se definida como vazio */
        /* ex.: n�o tem vari�veis nos registradores   */
#else
        Boolean Reg = true;
#endif

/* Vari�veis para medi��o de tempo:
 * apenas se compilar em ambientes que suportem
 * a fun��o TIMES. Se n�o utilizar vou tirar do c�digo no futuro */

#ifdef TIMES
        struct tms *time_info;
        extern int times();  /* mais informa��es na biblioteca "times" */
		#define Too_Small_Time 120 /* as medi��es devem ser ao menos com tempo de 2 segundos */
#endif

/* Fun��o TIME utilizada na compila��o do MinGW no Windows 7 */
#ifdef TIME
        extern long time(); /* mais informa��es na biblioteca "time"  */
		#define Too_Small_Time 2 /* as medi��es devem ser ao menos com tempo de 2 segundos */
#endif

/* Processo de atribui��o de estruturas,
 * se o compilador C utilizado n�o suportar
*/
#ifdef  NOSTRUCTASSIGN
		memcpy (d, s, l)
		register char   *d;
		register char   *s;
		register int    l;
		{
			while (l--) *d++ = *s++;
		}
#endif

double Begin_Time, End_Time, User_Time; //long � a medida no c�digo original
double Microseconds, Dhrystones_Per_Second, Vax_Mips;; // e float nessas vari�veis

/* fim das vari�veis de medi��o do tempo */

/* in�cio dos procedimentos de testes */
void Proc_1(Ptr_Val_Par)
REG Rec_Pointer Ptr_Val_Par;
{
	REG Rec_Pointer Next_Record = Ptr_Val_Par->Ptr_Comp;
	/* Vari�veis locais, inicializadas com Ptr_Val_Par->Ptr_Comp,    */

	structassign (*Ptr_Val_Par->Ptr_Comp, *Ptr_Glob);
	Ptr_Val_Par->variant.var_1.Int_Comp = 5;
	Next_Record->variant.var_1.Int_Comp = Ptr_Val_Par->variant.var_1.Int_Comp;
	Next_Record->Ptr_Comp = Ptr_Val_Par->Ptr_Comp;
	Proc_3(&Next_Record->Ptr_Comp);

	if (Next_Record->Discr == Ident_1) /* executa */
	{
		Next_Record->variant.var_1.Int_Comp = 6;
		Proc_6(Ptr_Val_Par->variant.var_1.Enum_Comp, &Next_Record->variant.var_1.Enum_Comp);
		Next_Record->Ptr_Comp = Ptr_Glob->Ptr_Comp;
		Proc_7(Next_Record->variant.var_1.Int_Comp, 10, &Next_Record->variant.var_1.Int_Comp);
	}
	else /* n�o executa */
		structassign(*Ptr_Val_Par, *Ptr_Val_Par->Ptr_Comp);
}


void Proc_2(Int_Par_Ref) //executa apenas uma vez e a vari�vel Int_Par_Ref de 1 torna-se 4
One_Fifty *Int_Par_Ref;
{
	One_Fifty Int_Loc;
	Enumeration Enum_Loc;

	Int_Loc = *Int_Par_Ref + 10;
	do /* executa apenas uma vez */
	{
		if (Ch_1_Glob == 'A') /* ent�o executa */
		{
			Int_Loc -= 1;
			*Int_Par_Ref = Int_Loc - Int_Glob;
			Enum_Loc = Ident_1;
		} //fim do if
	} //fim do
	while (Enum_Loc != Ident_1); /* enquanto verdade "true" */
}


void Proc_3(Ptr_Ref_Par) // executa apenas uma vez e Ptr_Ref_Par torna-se Ptr_Glob
Rec_Pointer *Ptr_Ref_Par;
{
	if (Ptr_Glob != Null)
	{
		*Ptr_Ref_Par = Ptr_Glob->Ptr_Comp;
	}
	Proc_7(10, Int_Glob, &Ptr_Glob->variant.var_1.Int_Comp); //chama Proc_7 definido em "dhry2.c"
}


void Proc_4() /* executa apenas uma vez sem par�metros */
{
	Boolean Bool_Loc;

	Bool_Loc = Ch_1_Glob == 'A'; //apenas um teste de verdade
	Bool_Glob = Bool_Loc | Bool_Glob; //se true ou true a vari�vel boleana global � true
	Ch_2_Glob = 'B';
}


void Proc_5() /* executa apenas uma vez sem par�metros assim como Proc_4()*/
{
	Ch_1_Glob = 'A';
	Bool_Glob = false;
}

/* Fun��o para c�lculo da velocidade do processador */
double dtime()
{
	#define HZ CLOCKS_PER_SEC
	clock_t tnow;

	double q;
	tnow = clock();
	q = (double)tnow / (double)HZ;
	return q;
}

/* Informa��es sobre hardware */
long getClockProcessador()
{
	DWORD tam = 255;
    DWORD tipo = 0;
    HKEY hkey;
    long clock;

    UCHAR *btmp = malloc(tam*sizeof(*btmp));
    RegOpenKey(HKEY_LOCAL_MACHINE, KEY_PROCESSADOR, &hkey); //abre o registro para pegar informa��es do clock
    RegQueryValueEx(hkey, "~MHz", 0, &tipo, btmp, &tam); //pega o valor do clock do processador na variavel do registro ~MHz
    RegCloseKey(hkey);
    memcpy((void*)&clock, (void*)btmp, sizeof(long)); //copia o valor do clock encontrado no registro para buffer
    free (btmp);
    return clock;  //retorna o clock do processador em MHz
}


char* infoProcessador(char *buffer)
{
	DWORD tam = 255;
    DWORD tipo = 0;
    HKEY hkey;

    UCHAR *btmp = malloc(tam*sizeof(btmp));
    UCHAR *ptr = btmp;
    RegOpenKey(HKEY_LOCAL_MACHINE, KEY_PROCESSADOR, &hkey); // abre o registro para pegar a stringo do processador
    RegQueryValueEx(hkey, "ProcessorNameString", 0, &tipo, btmp, &tam); //pega a string do processador na variavel do registro ProcessorNameString
    RegCloseKey(hkey);
    while (*(++btmp) == ' ');
    strcpy(buffer, (char*)btmp);
    free (ptr);
    return buffer; // retorna string do processador
}
/*Fim informa��es sobre o hardware */

/* In�cio do main corresponde ao Proc_0 na linguagem ADA */
int main(int argc, char *argv[])
{
			One_Fifty       Int_1_Loc;
	  REG   One_Fifty       Int_2_Loc;
			One_Fifty       Int_3_Loc;
	  REG   char            Ch_Index;
			Enumeration     Enum_Loc;
			Str_30          Str_1_Loc;
			Str_30          Str_2_Loc;
	  REG   int             Run_Index;
	  REG   int             Number_Of_Runs;

	  /*inicio de informa��es para identifica��o de hardware */
	  SYSTEM_INFO si;
	  MEMORYSTATUS ms;
	  PERFORMACE_INFORMATION pi;

	  GetSystemInfo(&si);
	  GlobalMemoryStatus(&ms);
	  ms.dwLength = sizeof(ms);

	  /* Inicializa��o de vari�veis de uso de fun��es do desenvolvedor */
	  FILE *Ap;
	  double dtime();
	  int count = 10;

	  if (argc > 1)
	  {
		  switch (argv[1][0])
	      {
		  case 'N':
			  flag = 0;
			  break;
		  case 'n':
			  flag = 0;
			  break;
	      } //fim do switch - odeio if!!!!
	  } //fim do if

	  //verifica se existe o arquivo de armazenagem dos resultados
	  if ((Ap = fopen("report.txt","a+")) == NULL)
	  {
		  printf("Sem acesso ao report.txt\n\n");
	      //printf("Aperte alguma tecla\n");
	      //endit = getch();
	      exit(1);
	  }

	  /* Inicializa��o */
	  Next_Ptr_Glob = (Rec_Pointer) malloc (sizeof (Rec_Type));
	  Ptr_Glob = (Rec_Pointer) malloc (sizeof (Rec_Type));

	  Ptr_Glob->Ptr_Comp                    = Next_Ptr_Glob;
	  Ptr_Glob->Discr                       = Ident_1;
	  Ptr_Glob->variant.var_1.Enum_Comp     = Ident_3;
	  Ptr_Glob->variant.var_1.Int_Comp      = 40;
	  strcpy (Ptr_Glob->variant.var_1.Str_Comp, "CPU TESTE");
	  strcpy (Str_1_Loc, "PRIMEIRA STRING");

	  Arr_2_Glob [8][7] = 10;
        /* Sem essa declara��o,
        * Arr_2_Glob [8][7] teria um valor indefinido.
        * Aten��o: Com processadores de 16-Bits e Number_Of_Runs > 32000,
        * overflow pode ocorrer para este elemento da matriz.
        * */

	  printf ("\n");
	  printf ("Dhrystone Benchmark, Versao 2.2\n");
	  printf ("\n");

	  if (flag == 0)
	  {
		  printf ("Sem dados de entrada em tempo de execucao\n\n");
	  }
	  else
	  {
		  printf ("Com dados de entrada definidos\n\n");
	  }

	  printf ("Compilador usado Cygwin - gcc 5.4.0 - win32\n");
	  printf ("\nOpcoes: -O02 -g3 -Wall -c -fmessage-length=0 -pg -DTIME\n");

	  #ifdef ROPT
	  	  printf ("\nOpcao por registradores ROPT\n\n");
	  #else
	      printf ("\nSem opcao por registradores\n\n");
	      strcpy(Reg_Define, "\nOpcao por registradores nao selecionada.");
	  #endif

	  /**********************/
      /* Inicio da execucao */
      /**********************/
	  Number_Of_Runs = 500000000;

	  do
	  {
		  Number_Of_Runs = Number_Of_Runs * 2;
		  count = count - 1;
		  Arr_2_Glob [8][7] = 10;

		  Begin_Time = dtime();


		  for (Run_Index = 1; Run_Index <= Number_Of_Runs; ++Run_Index)
		  {
			  Proc_5();
			  Proc_4(); // Vari�veis Ch_1_Glob == 'A', Ch_2_Glob == 'B', Bool_Glob == true
			  Int_1_Loc = 2;
			  Int_2_Loc = 3;
			  strcpy (Str_2_Loc, "SEGUNDA STRING");
			  Enum_Loc = Ident_2;
			  Bool_Glob = ! Func_2 (Str_1_Loc, Str_2_Loc); // Aqui Bool_Glob == 1
			  while (Int_1_Loc < Int_2_Loc)  // executa apenas uma vez
			  {
				  Int_3_Loc = 5 * Int_1_Loc - Int_2_Loc; // Int_3_Loc == 7
				  Proc_7 (Int_1_Loc, Int_2_Loc, &Int_3_Loc); // Int_3_Loc == 7
				  Int_1_Loc += 1;
			  } //fim do while
			  /* Agora Int_1_Loc == 3, Int_2_Loc == 3, Int_3_Loc == 7 */
			  Proc_8(Arr_1_Glob, Arr_2_Glob, Int_1_Loc, Int_3_Loc); // Int_Glob == 5
			  Proc_1(Ptr_Glob);
			  for (Ch_Index = 'A'; Ch_Index <= Ch_2_Glob; ++Ch_Index)  // executa duas vezes
			  {
				  if (Enum_Loc == Func_1 (Ch_Index, 'C')) // se true ent�o n�o executa
				  {
					  Proc_6(Ident_1, &Enum_Loc);
					  strcpy (Str_2_Loc, "TERCEIRA STRING");
					  Int_2_Loc = Run_Index;
					  Int_Glob = Run_Index;
				  }
			  }
			  /* Agora Int_1_Loc == 3, Int_2_Loc == 3, Int_3_Loc == 7 */
			  Int_2_Loc = Int_2_Loc * Int_1_Loc;
			  Int_1_Loc = Int_2_Loc / Int_3_Loc;
			  Int_2_Loc = 7 * (Int_2_Loc - Int_3_Loc) - Int_1_Loc; /* Int_1_Loc == 1, Int_2_Loc == 13, Int_3_Loc == 7 */
			  Proc_2 (&Int_1_Loc); /* Int_1_Loc == 5 */

		  } // fim do loop "for Run_Index"

		  /*******************/
		  /* Fim da execucao */
		  /*******************/

		  End_Time = dtime();
		  User_Time = End_Time - Begin_Time;

		  printf ("%12.0f execucoes %6.2f segundos \n",(double) Number_Of_Runs, User_Time);
		  if (User_Time > 5)
		  {
			  count = 0;
		  }
		  else
		  {
			  if (User_Time < 0.1)
			  {
				  Number_Of_Runs = Number_Of_Runs * 5;
			  }
		  }
	  }   // fim do-while
	  while (count >0);

	  printf ("Fim da execucao\n");
	  printf ("\n");

	  if (User_Time < Too_Small_Time)
	  {
	  	  printf ("\nMedida de tempo muito pequeno para obter resultados significativos");
	  	  printf ("\nPor favor, aumente o numero de runs\n");
	  	  printf ("\n");
	  }
	  else
	  {
	  	  #ifdef TIME
	  	  	  Microseconds = (double) User_Time * Mic_secs_Per_Second / (double) Number_Of_Runs;
	  	  	  Dhrystones_Per_Second = (double) Number_Of_Runs / (double) User_Time;
	  	  	  Vax_Mips = Dhrystones_Per_Second / 1757.0;
	  	  #else
	  	  	  Microseconds = (float) User_Time * Mic_secs_Per_Second / ((float) HZ * ((float) Number_Of_Runs));
	  	  	  Dhrystones_Per_Second = ((float) HZ * (float) Number_Of_Runs) / (float) User_Time;
	  	  #endif

	  }

	  //inicio modif
	  //printf ("Deseja imprimir em tela resultado obtido? (s/n)\n");

	  //char resp;
	  //int ctrflag = 1;

	  //while (ctrflag)
	  //{
		  //scanf ("%c",&resp);

		  //if (resp == 'S' || resp == 's')
		  //{
			  printf ("Valores finais:\n");
			  printf ("\n");
			  printf ("Int_Glob:            %d\n", Int_Glob);
			  printf ("        deve ser:    %d\n", 5);
			  printf ("Bool_Glob:           %d\n", Bool_Glob);
			  printf ("        deve ser:    %d\n", 1);
			  printf ("Ch_1_Glob:           %c\n", Ch_1_Glob);
			  printf ("        deve ser:    %c\n", 'A');
			  printf ("Ch_2_Glob:           %c\n", Ch_2_Glob);
			  printf ("        deve ser:    %c\n", 'B');
			  printf ("Arr_1_Glob[8]:       %d\n", Arr_1_Glob[8]);
			  printf ("        deve ser:    %d\n", 7);
			  printf ("Arr_2_Glob[8][7]:    %d\n", Arr_2_Glob[8][7]);
			  printf ("        deve ser:    %d\n", Number_Of_Runs + 10);
			  printf ("Ptr_Glob->\n");
			  printf ("  Discr:             %d\n", Ptr_Glob->Discr);
			  printf ("        deve ser:    %d\n", 0);
			  printf ("  Enum_Comp:         %d\n", Ptr_Glob->variant.var_1.Enum_Comp);
			  printf ("        deve ser:    %d\n", 2);
			  printf ("  Int_Comp:          %d\n", Ptr_Glob->variant.var_1.Int_Comp);
			  printf ("        deve ser:    %d\n", 17);
			  printf ("  Str_Comp:          %s\n", Ptr_Glob->variant.var_1.Str_Comp);
			  printf ("        deve ser:    CPU TESTE\n");
			  printf ("  Discr:             %d\n", Next_Ptr_Glob->Discr);
			  printf ("        deve ser:    %d\n", 0);
			  printf ("  Enum_Comp:         %d\n", Next_Ptr_Glob->variant.var_1.Enum_Comp);
			  printf ("        deve ser:    %d\n", 1);
			  printf ("  Int_Comp:          %d\n", Next_Ptr_Glob->variant.var_1.Int_Comp);
			  printf ("        deve ser:    %d\n", 18);
			  printf ("  Str_Comp:          %s\n", Next_Ptr_Glob->variant.var_1.Str_Comp);
			  printf ("        deve ser:    CPU TESTE\n");
			  printf ("Int_1_Loc:           %d\n", Int_1_Loc);
			  printf ("        deve ser:    %d\n", 5);
			  printf ("Int_2_Loc:           %d\n", Int_2_Loc);
			  printf ("        deve ser:    %d\n", 13);
			  printf ("Int_3_Loc:           %d\n", Int_3_Loc);
			  printf ("        deve ser:    %d\n", 7);
			  printf ("Enum_Loc:            %d\n", Enum_Loc);
			  printf ("        deve ser:    %d\n", 1);
			  printf ("Str_1_Loc:           %s\n", Str_1_Loc);
			  printf ("        deve ser:    PRIMEIRA STRING\n");
			  printf ("Str_2_Loc:           %s\n", Str_2_Loc);
			  printf ("        deve ser:    SEGUNDA STRING\n");
			  printf ("\n");

			  printf ("Microssegundos para uma execucao com o Dhrystone: %12.2lf\n", Microseconds);
			  printf ("Dhrystones por segundos:                          %10.0lf\n", Dhrystones_Per_Second);
			  printf ("Taxa VAX  MIPS:                                   %12.2lf\n", Vax_Mips);
			  printf ("Resultados salvos no arquivo report.txt.\n");
			  printf ("\n");

			  //ctrflag = 0;
		  //}
		  //else
		  //{
			  //if (resp == 'N' || resp == 'n')
			  //{
				  //printf ("Sem impressao na tela.\nResultados salvos no arquivo report.txt.\n");
			      //ctrflag = 0;
			  //}
			  //else
			  //{
				  //ctrflag = 1;
				  //printf ("Opcao invalida - Use (s/n)\n");
			  //}
		  //}
	  //}

  	  char buffer[255] = { 0 };

	  fprintf (Ap, "-------------------- -----------------------------------\n");
	  fprintf (Ap, "Dhrystone Benchmark Versao 2.2 (Linguagem C)\n\n");
	  fprintf (Ap, "DATA : %s HORA: %s\n",__DATE__,__TIME__);
	  fprintf (Ap, "Fornecedor/Modelo do PC:      Dell\n");
	  fprintf (Ap, "CPU:                          %s\n", infoProcessador(buffer));
	  fprintf (Ap, "Clock MHz:                    %d\n", getClockProcessador());
	  fprintf (Ap, "Cache de pagina Mbytes:       %lu\n", (long unsigned int) pi.SystemCache / (1024 * 1024));
	  fprintf (Ap, "Arquitetura do processador:   %lu\n", (long unsigned int) si.wProcessorArchitecture);
	  fprintf (Ap, "Versao do Windows OEM ID:     %u\n", si.dwOemId);
	  fprintf (Ap, "\nCompilador Cygwin - gcc 5.4.0 - win32\n"); //colocar compile
	  fprintf (Ap, "Otimizacao -O02 -g3 -Wall -c -fmessage-length=0 -pg -DTIME\n"); //colocar option
	  fprintf (Ap, "Executado por:                Anderson Moreira\n");
	  fprintf (Ap, "Organizacao:                  IFPE - Recife\n");
	  fprintf (Ap, "Mail:                         anderson.moreira@recife.ifpe.edu.br\n");
	  fprintf (Ap, "\nNumero de Execucoes:        %12.0f\n", (double) Number_Of_Runs);
	  fprintf (Ap, "Tempo decorrido da execucao:  %6.2f segundos \n",User_Time);

	  fprintf (Ap, "\nValores Finais:\n");
	  fprintf (Ap, "\n");
	  fprintf (Ap, "Int_Glob:      ");
	  if (Int_Glob == 5)  fprintf (Ap, "O.K.  ");
	  else                fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Int_Glob);

	  fprintf (Ap, "Bool_Glob:     ");
	  if (Bool_Glob == 1) fprintf (Ap, "O.K.  ");
	  else                fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Bool_Glob);

	  fprintf (Ap, "Ch_1_Glob:     ");
	  if (Ch_1_Glob == 'A')  fprintf (Ap, "O.K.  ");
	  else                   fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%c\n", Ch_1_Glob);

	  fprintf (Ap, "Ch_2_Glob:     ");
	  if (Ch_2_Glob == 'B')  fprintf (Ap, "O.K.  ");
	  else                   fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%c\n",  Ch_2_Glob);

	  fprintf (Ap, "Arr_1_Glob[8]: ");
	  if (Arr_1_Glob[8] == 7)  fprintf (Ap, "O.K.  ");
	  else                     fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Arr_1_Glob[8]);

	  fprintf (Ap, "Arr_2_Glob8/7: ");
	  if (Arr_2_Glob[8][7] == Number_Of_Runs + 10)  fprintf (Ap, "O.K.  ");
	  else							                fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%10d\n", Arr_2_Glob[8][7]);

	  fprintf (Ap, "Ptr_Glob->  \n");
	  fprintf (Ap, "  Ptr_Comp:       *  %d\n", (int) Ptr_Glob->Ptr_Comp);
	  fprintf (Ap, "  Discr:       ");
	  if (Ptr_Glob->Discr == 0)  fprintf (Ap, "O.K.  ");
	  else                       fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Ptr_Glob->Discr);

	  fprintf (Ap, "  Enum_Comp:   ");
	  if (Ptr_Glob->variant.var_1.Enum_Comp == 2) fprintf (Ap, "O.K.  ");
	  else           						      fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Ptr_Glob->variant.var_1.Enum_Comp);

	  fprintf (Ap, "  Int_Comp:    ");
	  if (Ptr_Glob->variant.var_1.Int_Comp == 17)  fprintf (Ap, "O.K.  ");
	  else                                         fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Ptr_Glob->variant.var_1.Int_Comp);

	  fprintf (Ap, "  Str_Comp:    ");
	  if (strcmp(Ptr_Glob->variant.var_1.Str_Comp,"CPU TESTE") == 0)  fprintf (Ap, "O.K.  ");
	  else         																       fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%s\n", Ptr_Glob->variant.var_1.Str_Comp);

	  fprintf (Ap, "Next_Ptr_Glob-> \n");
	  fprintf (Ap, "  Ptr_Comp:       *  %d", (int) Next_Ptr_Glob->Ptr_Comp);
	  fprintf (Ap, " mesmo que o anterior\n");

	  fprintf (Ap, "  Discr:       ");
	  if (Next_Ptr_Glob->Discr == 0)  fprintf (Ap, "O.K.  ");
	  else            			      fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Next_Ptr_Glob->Discr);

	  fprintf (Ap, "  Enum_Comp:   ");
	  if (Next_Ptr_Glob->variant.var_1.Enum_Comp == 1)  fprintf (Ap, "O.K.  ");
	  else          								    fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Next_Ptr_Glob->variant.var_1.Enum_Comp);

	  fprintf (Ap, "  Int_Comp:    ");
	  if (Next_Ptr_Glob->variant.var_1.Int_Comp == 18)	fprintf (Ap, "O.K.  ");
	  else            								    fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Next_Ptr_Glob->variant.var_1.Int_Comp);

	  fprintf (Ap, "  Str_Comp:    ");
	  if (strcmp(Next_Ptr_Glob->variant.var_1.Str_Comp,"CPU TESTE") == 0)  fprintf (Ap, "O.K.  ");
	  else     																		        fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%s\n", Next_Ptr_Glob->variant.var_1.Str_Comp);

	  fprintf (Ap, "Int_1_Loc:     ");
	  if (Int_1_Loc == 5)  fprintf (Ap, "O.K.  ");
	  else                 fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Int_1_Loc);

	  fprintf (Ap, "Int_2_Loc:     ");
	  if (Int_2_Loc == 13)  fprintf (Ap, "O.K.  ");
	  else       	        fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Int_2_Loc);

	  fprintf (Ap, "Int_3_Loc:     ");
	  if (Int_3_Loc == 7)    fprintf (Ap, "O.K.  ");
	  else     	             fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Int_3_Loc);

	  fprintf (Ap, "Enum_Loc:      ");
	  if (Enum_Loc == 1)  fprintf (Ap, "O.K.  ");
	  else                fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%d\n", Enum_Loc);

	  fprintf (Ap, "Str_1_Loc:     ");
	  if (strcmp(Str_1_Loc, "PRIMEIRA STRING") == 0)  fprintf (Ap, "O.K.  ");
	  else    											             fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%s\n", Str_1_Loc);

	  fprintf (Ap, "Str_2_Loc:     ");
	  if (strcmp(Str_2_Loc, "SEGUNDA STRING") == 0)  fprintf (Ap, "O.K.  ");
	  else  											             fprintf (Ap, "FALHA ");
	  fprintf (Ap, "%s\n", Str_2_Loc);


	  fprintf (Ap, "\n");
	  fprintf(Ap,"%s\n",Reg_Define);
	  fprintf (Ap, "\n");
	  fprintf(Ap,"Microssegundos para 1 loop:   %12.2lf\n",Microseconds);
	  fprintf(Ap,"Dhrystones / segundos:        %10.0lf\n",Dhrystones_Per_Second);
	  fprintf(Ap,"Taxa VAX MIPS:                %12.2lf\n\n",Vax_Mips);
	  fclose(Ap);


      if (flag == 1)
      {
    	  printf("Pressione alguma tecla para sair\n");
          printf ("\nSe essa mensagem aparecer feche a tela normalmente\n");
      }
      return 0;
}//fim do main

/* Procedimento para a atribui��o de estruturas,          */
/* se o compilador C n�o oferece suporte a este recurso   */
#ifdef  NOSTRUCTASSIGN
memcpy (d, s, l)
register char   *d;
register char   *s;
register int    l;
{
	while (l--) *d++ = *s++;
}
#endif
